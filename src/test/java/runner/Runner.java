package runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import io.cucumber.testng.TestNGCucumberRunner;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.Reportable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CucumberOptions(
        plugin = {"pretty", "html: target/cucumber.html", "json:target/cucumber.json"},
        features = "src/test/resources",
        glue = {"stepDef", "utils"}

)
public class Runner extends AbstractTestNGCucumberTests {

    private final Logger logger = LogManager.getLogger(Runner.class);
    private static TestNGCucumberRunner testNGCucumberRunner;

    @BeforeSuite(alwaysRun = true)
    public void setupSuite() throws IOException {
        logger.info("Done with BeforeSuite setup! TESTS BEGINNING!\n\n");
    }

    @BeforeClass(alwaysRun = true)
    public void setupClass() {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        testNGCucumberRunner.finish();
    }

    @AfterSuite(alwaysRun = true)
    public void cleanUp() {

        generatePrettyReportsLocally();
        logger.info("TESTING COMPLETE: SHUTTING DOWN FRAMEWORK!!");
    }

    private void generatePrettyReportsLocally() {
        String projectName = "SQLProject02TestNGFrameworkForDBTesting";
        String reportFilePath = "target";
        File reportOutputDirectory = new File(reportFilePath);
        List<String> jsonFiles = new ArrayList<>();
        jsonFiles.add("target/cucumber.json");

        Configuration configuration = new Configuration(reportOutputDirectory, projectName);

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        Reportable result = reportBuilder.generateReports();

        logger.info("\n**************************************************************************************************************" +
                "\n--------------------------------------   LOCAL PRETTY REPORT CREATED   ----------------------------------------" +
                "\nLink: http://localhost:63342/" + projectName + "/" + reportFilePath + "/cucumber-html-reports/overview-features.html" +
                "\n---------------------------------------------------------------------------------------------------------------" +
                "\n***************************************************************************************************************" );
    }
}
